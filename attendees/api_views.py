from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json

# the .models will import the models.py file from the same directory.
# In case you need to rename your directory
from .models import Attendee


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    # attendees = [
    #     {
    #         "name": a.name,
    #         "href": a.get_api_url(),
    #     }
    #     for a in Attendee.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"attendees": attendees})


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            # new code
            if "attendee" in content:
                attendee = Attendee.objects.get(id=pk)
                content["attendee"] = attendee
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee id"},
                status=400,
            )
        Attendee.objects.filter(id=pk).update(**content)
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    # attendee_details = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": attendee_details.email,
    #         "name": attendee_details.name,
    #         "company_name": attendee_details.company_name,
    #         "created": attendee_details.created,
    #         "conference": {
    #             "name": attendee_details.conference.name,
    #             "href": attendee_details.conference.get_api_url(),
    #         },
    #     }
    # )
