from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_pexel_image(city, state):

    url = "https://api.pexels.com/v1/search"
    headers = dict("Authorization", PEXELS_API_KEY)

    r = requests.get(url, headers=headers)


def get_weather(city, state):

    url = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}"
    headers = dict("Authorization", OPEN_WEATHER_API_KEY)

    r = requests.get(url, headers=headers)
